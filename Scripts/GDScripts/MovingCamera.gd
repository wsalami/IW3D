extends Spatial

## CONSTANTS ##
const CAM_SPEED = 0.15


var offset = Vector3(0,0,0)

func _physics_process(delta):
	offset = Vector3(0,0,0)
	
	if Input.is_action_pressed("ui_left"):
		offset.x = -CAM_SPEED
	if Input.is_action_pressed("ui_right"):
		offset.x = CAM_SPEED
	if Input.is_action_pressed("ui_up"):
		offset.z = -CAM_SPEED
	if Input.is_action_pressed("ui_down"):
		offset.z = CAM_SPEED
	
	
	
	translate(offset)
