extends KinematicBody

## CONSTANTS ##
const ray_length = 100

var speed = 10
var pos = global_transform.origin
var dest = global_transform.origin
var vel = Vector3()

func _physics_process(delta):
	
	pos = global_transform.origin
	
	vel = pos.move_toward(dest, speed) - pos
	
	
	move_and_slide(vel)



func _on_StaticBody_move_click(click_position):
	dest  = click_position
