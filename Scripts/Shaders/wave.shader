shader_type spatial;

render_mode specular_toon;

uniform float transp;
uniform sampler2D noise;
uniform float PlaneSize;
uniform vec4 out_color : hint_color = vec4(0.0, 0.2, 1.0, 1.0);
varying vec2 vertex_position;

float wave(vec2 position){
  position += texture(noise, position / PlaneSize).x * 2.0 - 1.0;
  vec2 wv = 1.0 - abs(sin(position));
  return pow(1.0 - pow(wv.x * wv.y, 0.65), 4.0);
}

float height(vec2 position, float time) {
  float d = wave((position + time) * 0.4) * 0.3;
  d += wave((position - time) * 0.3) * 0.3;
  d += wave((position + time) * 0.5) * 0.2;
  d += wave((position - time) * 0.6) * 0.2;
  return d;
}

void vertex() {
  vertex_position = VERTEX.xz;
  float k = height(vertex_position, TIME/1.5); //Divide by the size of the PlaneMesh
  VERTEX.y += k;
}

void fragment() {
  NORMAL = normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));
  float fresnel = sqrt(1.0 - dot(NORMAL, VIEW));
  RIM = 0.2;
  METALLIC = 0.1;
  ROUGHNESS = 0.01 * (1.0 - fresnel);
  ALBEDO = out_color.xyz + (0.1 * fresnel);
}